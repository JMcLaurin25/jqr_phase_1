
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "gameboard.h"
#include "gameboard_functs.h"

char ** task1(int x_coord, int y_coord)
{
	if (x_coord < 10 || y_coord < 10) {
		printf("Board dimensions cannot be less than 10 in either direction.\n");
		return 0;
	}

	char **board = (char **) calloc(y_coord, sizeof(char*));
	int sz_board[] = {x_coord, y_coord};

	for (int index = 0; index < y_coord; index++) {
		board[index] = (char *) calloc(x_coord, sizeof(char));
		if(!board[index]) {
			printf("Error allocating column\n");
		}
	}

	fill_water(board, sz_board);
	//Ship building below

	/* Patrol boat */
	struct boat *patrol = malloc(sizeof(struct boat));
	if (!patrol) {
		printf("Failed\n");
	} 
	make_boat(patrol, 2, 'P');

	/* Submarine boat */
	struct boat *submarine = malloc(sizeof(struct boat));
	if (!submarine) {
		printf("Failed\n");
	} 
	make_boat(submarine, 2, 'S');

	/* cruiser boat */
	struct boat *cruiser = malloc(sizeof(struct boat));
	if (!cruiser) {
		printf("Failed\n");
	} 
	make_boat(cruiser, 3, 'C');

	/* destroyer boat */
	struct boat *destroyer = malloc(sizeof(struct boat));
	if (!destroyer) {
		printf("Failed\n");
	} 
	make_boat(destroyer, 3, 'D');

	/* battleship boat */
	struct boat *battleship = malloc(sizeof(struct boat));
	if (!battleship) {
		printf("Failed\n");
	} 
	make_boat(battleship, 4, 'B');

	/* aircraft_carrier boat */
	struct boat *aircraft_carrier = malloc(sizeof(struct boat));
	if (!aircraft_carrier) {
		printf("Failed\n");
	} 
	make_boat(aircraft_carrier, 5, 'A');

	place_boat(board, aircraft_carrier, sz_board);
	place_boat(board, battleship, sz_board);
	place_boat(board, destroyer, sz_board);
	place_boat(board, cruiser, sz_board);
	place_boat(board, patrol, sz_board);
	place_boat(board, submarine, sz_board);

	free(aircraft_carrier);
	free(battleship);
	free(destroyer);
	free(cruiser);
	free(patrol);
	free(submarine);

	return board;
}


void save_board(const char *target_file, char **board, int x, int y)
{
	FILE *target = fopen(target_file, "w");

	for (int x_index = 0; x_index < x; x_index++) {
		for (int y_index = 0; y_index < y; y_index++) {
			fprintf(target, "%c", board[x_index][y_index]);
		}
		fprintf(target, "%c", '\n');
	}
	fclose(target);
}

char **open_board(const char *target_file, int *sz_board)
{
	char **board;
	char curChar;

	FILE *source = fopen(target_file, "r");
	if (!source) {
		printf("File creation failed\n");
		exit (0);
	}

	get_file_dimensions(source, &sz_board[0], &sz_board[1]);

	if (sz_board[0] < 10 || sz_board[1] < 10) {
		printf("Board dimensions cannot be less than 10 in either direction.\n");
		exit (0);
	}

	board = (char **) calloc(sz_board[1], sizeof(char*));

	for (int index = 0; index < sz_board[1]; index++) {
		board[index] = (char *) calloc(sz_board[0], sizeof(char));
		if(!board[index]) {
			printf("Error allocating column\n");
		}
	}

	int index1 = 0, index2 = 0;
	rewind(source);
	while((curChar = fgetc(source)) != EOF) {
		
		if (curChar == '\n') {
			index1++;
			index2 = 0;
		} else {
			board[index1][index2] = curChar;

			index2++;
		}
	}

	fclose(source);
	return board;
}

void fire(char **board, struct scores *ship_health, int *x_coord, int *y_coord, int *sunk)
{
	switch (board[*x_coord][*y_coord]) {
		case 'P': //Patrol boat 2
			printf("HIT!!\n");
			ship_health->patrol += 1;
			if (ship_health->patrol == 2) {
				printf("You sank my BattleShip!\n");
				*sunk += 1;
			}
			printf("Ships remaining: %d\n", 6 - *sunk);
			board[*x_coord][*y_coord] = 'X';
			break;
		case 'S': //Submarine 2
			printf("HIT!!\n");
			ship_health->submarine += 1;
			if (ship_health->submarine == 2) {
				printf("You sank my BattleShip!\n");
				*sunk += 1;
			}
			printf("Ships remaining: %d\n", 6 - *sunk);
			board[*x_coord][*y_coord] = 'X';
			break;
		case 'C': //Cruiser 3
			printf("HIT!!\n");
			ship_health->cruiser += 1;
			if (ship_health->cruiser == 3) {
				printf("You sank my BattleShip!\n");
				*sunk += 1;
			}
			printf("Ships remaining: %d\n", 6 - *sunk);
			board[*x_coord][*y_coord] = 'X';
			break;
		case 'D': //Destroyer 3
			printf("HIT!!\n");
			ship_health->destroyer += 1;
			if (ship_health->destroyer == 3) {
				printf("You sank my BattleShip!\n");
				*sunk += 1;
			}
			printf("Ships remaining: %d\n", 6 - *sunk);
			board[*x_coord][*y_coord] = 'X';
			break;
		case 'B': //Battleship 4
			printf("HIT!!\n");
			ship_health->battleship++;
			if (ship_health->battleship == 4) {
				printf("You sank my BattleShip!\n");
				*sunk += 1;
			}
			printf("Ships remaining: %d\n", 6 - *sunk);
			board[*x_coord][*y_coord] = 'X';
			break;
		case 'A': //Aircraft Carrier 5
			printf("HIT!!\n");
			ship_health->aircraft_carrier += 1;
			if (ship_health->aircraft_carrier == 5) {
				printf("You sank my BattleShip!\n");
				*sunk += 1;
			}
			printf("Ships remaining: %d\n", 6 - *sunk);
			board[*x_coord][*y_coord] = 'X';
			break;
		case '*': //Water
			printf("MISS!!\n");
			printf("Ships remaining: %d\n", 6 - *sunk);
			board[*x_coord][*y_coord] = 'M';
			break;
		case 'M': //Miss
		case 'X': //Hit
			printf("Guess has already been made.\n");
			printf("Ships remaining: %d\n", 6 - *sunk);
			break;
		default:
			break;
	}
}

void playgame (char **gamefield, int *sz_board)
{
	int game = 0;

	int sunk = 0;
	char user_filename[24];
	int userguess[2] = {0};
	struct scores *ship_health;

	ship_health = malloc(sizeof(struct scores));
	memset(ship_health, 0, sizeof(struct scores));

	ship_health->patrol = 0;
	ship_health->submarine = 0;
	ship_health->cruiser = 0;
	ship_health->destroyer = 0;
	ship_health->battleship = 0;
	ship_health->aircraft_carrier = 0;

	get_ship_health(gamefield, sz_board, ship_health);

	print_board(gamefield, sz_board[0], sz_board[1]);

	while (game == 0){
		printf("'q' to quit, 's' to save.\n");
		printf("Make guess with format: 4,4\n Guess >> ");
		guess(userguess);
		if (userguess[0] == -1) {
			printf("Name of saved map: ");
			fgets(user_filename, sizeof(user_filename), stdin);
			user_filename[strlen(user_filename) - 1] = '\0';
			save_board(user_filename, gamefield, sz_board[0], sz_board[1]); //Task 2 - Want to pass 1 arg (filename) only.
			break;
		} else if (userguess[0] == -2) {
			break;
		} else if (userguess[0] == -3) {
			print_board(gamefield, sz_board[0], sz_board[1]);
			continue;
		} else {
			//Adjust for userinput
			userguess[0] -= 1;
			userguess[1] -= 1;
		}

		if ((userguess[0] >= 0 && userguess[0] <= sz_board[0]) && (userguess[1] >= 0 && userguess[1] <= sz_board[1])) {
			fire(gamefield, ship_health, &userguess[0], &userguess[1], &sunk);
			print_board(gamefield, sz_board[0], sz_board[1]);
		} else {
			printf("Invalid guess!\nGuess is not within the boards boundary\n");
		}
		if (sunk == 6) {
			game++;
			printf("WINNER! You have sunk all the Battleships!!\n");
		}
	}

	free(ship_health);

}

void print_board(char **board, int x, int y)
{
	int indx_i = 0, indx_j = 0;

	for (indx_i = 0; indx_i < x; indx_i++) {
		for (indx_j = 0; indx_j < y; indx_j++) {
			printf("%c", board[indx_i][indx_j]);
		}

		printf("\n");
	}
}

void guess(int *userguess) //Input user guess
{
	char input[12], *xdim, * ydim, *endptr;

	fgets(input, 12, stdin);
	input[strlen(input) - 1] = '\0';
	if(strcmp(input, "s") == 0) {
		userguess[0] = -1;
	} else if (strcmp(input, "q") == 0) {
		userguess[0] = -2;
	} else if (strstr(input, ",") == NULL) {
		printf("Invalid guess!\n");
		userguess[0] = -3;
	} else {
		xdim = strtok(input, ",");
		ydim = strtok(NULL, ",");
		if ((xdim == NULL) || (ydim == NULL)) {
			printf("Invalid guess!\n");
			userguess[0] = -3;
		} else {
			userguess[0] = strtof(xdim, &endptr);
			userguess[1] = strtof(ydim, &endptr);
		}
	}
}

void get_ship_health(char **board, int *sz_board, struct scores *ship_health)
{
	int indx_i = 0, indx_j = 0;

	for (indx_i = 0; indx_i < sz_board[0]; indx_i++) {
		for (indx_j = 0; indx_j < sz_board[1]; indx_j++) {
			switch (board[indx_i][indx_j]) {
				case 'P':
					ship_health->patrol += 1;
					break;
				case 'S':
					ship_health->submarine += 1;
					break;
				case 'C':
					ship_health->cruiser += 1;
					break;
				case 'D':
					ship_health->destroyer += 1;
					break;
				case 'B':
					ship_health->battleship += 1;
					break;
				case 'A':
					ship_health->aircraft_carrier += 1;
					break;
				default:
					break;
			}
		}
	}

	ship_health->patrol = 2 - ship_health->patrol;
	ship_health->submarine = 2 - ship_health->submarine;
	ship_health->cruiser = 3 - ship_health->cruiser;
	ship_health->destroyer = 3 - ship_health->destroyer;
	ship_health->battleship = 4 - ship_health->battleship;
	ship_health->aircraft_carrier = 5 - ship_health->aircraft_carrier;
}

void free_game(char ** board, int *sz_board)
{
	for (int index = sz_board[1] - 1; index >= 0; index--) {
		free(board[index]);
	}
	free(board);
}
