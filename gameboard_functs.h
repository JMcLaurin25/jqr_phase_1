#ifndef GAMEBOARD_FUNCTS_H
#define GAMEBOARD_FUNCTS_H

struct boat {
	int pos[2]; //Location of top left most point of boat
	int length; //Length of the vessel
	char symbol; //Character representation of the boat
	int direct; // 0 = Horizontal, 1 = Vertical
};

void fill_water(char **board, int *sz_board);

void print_boat(struct boat *boat);
void make_boat(struct boat *boat, int length, char symbol);
void place_boat(char **board, struct boat *boat, int *sz_board);
void get_file_dimensions(FILE *source, int *x_coord, int *y_coord);

#endif
