
#include <Python.h>

/*---------Objects and their arguments---------*/

static PyObject* UniversalError;

//"TASK 1: Add {} to number {}.", "INT:2"
static PyObject* task1(PyObject* aself, PyObject* args)
{
	int val1 = 0, val2 = 0;

	//Parse verification
	if (!PyArg_ParseTuple(args, "ii", &val1, &val2)) {
		PyErr_SetString(UniversalError, "Invalid Arguments\n");
		return NULL;
	}

	return Py_BuildValue("i", val1 + val2);
}

//"TASK 2: Divide {} by {}.", "INT:2"
static PyObject* task2(PyObject* aself, PyObject* args)
{
	int val1 = 0, val2 = 0;

	//Parse verification
	if (!PyArg_ParseTuple(args, "ii", &val1, &val2)) {
		PyErr_SetString(UniversalError, "Invalid Arguments\n");
		return NULL;
	}

	return Py_BuildValue("i", val1 / val2);
}

//"TASK 3: Multiply {} by {}.", "INT:2"
static PyObject* task3(PyObject* aself, PyObject* args)
{
	int val1 = 0, val2 = 0;

	//Parse verification
	if (!PyArg_ParseTuple(args, "ii", &val1, &val2)) {
		PyErr_SetString(UniversalError, "Invalid Arguments\n");
		return NULL;
	}

	return Py_BuildValue("i", val1 * val2);
}

//"TASK 4: Subtract {} from {}.", "INT:2"
static PyObject* task4(PyObject* aself, PyObject* args)
{
	int val1 = 0, val2 = 0;

	//Parse verification
	if (!PyArg_ParseTuple(args, "ii", &val1, &val2)) {
		PyErr_SetString(UniversalError, "Invalid Arguments\n");
		return NULL;
	}

	return Py_BuildValue("i", val1 - val2);
}

//"TASK 5: What is the remainder of {} divided by {}.", "INT:2"
static PyObject* task5(PyObject* aself, PyObject* args)
{
	int val1 = 0, val2 = 0;

	//Parse verification
	if (!PyArg_ParseTuple(args, "ii", &val1, &val2)) {
		PyErr_SetString(UniversalError, "Invalid Arguments\n");
		return NULL;
	}

	return Py_BuildValue("i", val1 % val2);
}


/*--------------Method Definitions--------------*/
static PyMethodDef JQRModule_Methods[] = {
	{"task1", task1, METH_VARARGS, "Adding 2 numbers."},
	{"task2", task2, METH_VARARGS, "Dividing 2 numbers."},
	{"task3", task3, METH_VARARGS, "Multiplying 2 numbers."},
	{"task4", task4, METH_VARARGS, "Subtracting 2 numbers."},
	{"task5", task5, METH_VARARGS, "Modulo of 2 numbers."},
	{NULL, NULL, METH_NOARGS, NULL}
};

/*--------------Module Definitions--------------*/
static struct PyModuleDef JQRModule = {
	PyModuleDef_HEAD_INIT,
	"Module for computing tasks.",
	"JQR Module",
	-1,
	JQRModule_Methods
};

/*---------Python Module Initialization---------*/
PyMODINIT_FUNC PyInit_JQRSolutions(void)
{
	PyObject* moduleObj	= PyModule_Create (&JQRModule);
	return moduleObj;
}
