
from distutils.core import setup, Extension

JQRModule = Extension('JQRSolutions', sources = ['JQRSolutions.c'])

setup( name= 'McLaurin - JQR Solutions',
		version = '1.0',
		description = 'This Module is for JQR Tasks.',
		author = 'McLaurin, James',
		url = 'http://www.internet.com',
		ext_modules = [JQRModule])

