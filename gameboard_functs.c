
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "gameboard_functs.h"

void fill_water(char **board, int *sz_board)
{
	int indx_i = 0, indx_j = 0;

	for (indx_i = 0; indx_i < sz_board[1]; indx_i++) {
		for (indx_j = 0; indx_j < sz_board[0]; indx_j++) {
			board[indx_i][indx_j] = '*';
		}
	}
}

void print_boat(struct boat *boat)
{
	printf("%dx%d: len=%d: symbol=%c: dir=%d\n", boat->pos[0], boat->pos[1], boat->length, boat->symbol, boat->direct);
}

void make_boat(struct boat *boat, int length, char symbol)
{
	boat->symbol = symbol;
	boat->length = length;
}

void place_boat(char **board, struct boat *boat, int *sz_board)
{
	int error = 0;
	boat->direct = 1; //Vertical

	while (error == 0) {
		srand(time(0)); //Random Number
		boat->pos[0] = rand() % sz_board[0]; //Random x coordinate
		boat->pos[1] = rand() % sz_board[1]; //Random y coordinate
		if (rand() % 2 != 0) {
			boat->direct = 0; //Horizontal
		}

		if (boat->direct == 0) {
			if (((boat->pos[0] + boat->length) >= sz_board[0]) || boat->pos[1] >= sz_board[1]) {
				error = 0;
			} else {
				//Check if all water
				int clear_water = 0;

				for (int index = 0; index < boat->length; index++) {
					if (board[boat->pos[0]][boat->pos[0] + index] != '*') {
						clear_water = 1;
					}
				}

				if (clear_water == 0) {
					for (int index = 0; index < boat->length; index++) {
						board[boat->pos[0]][boat->pos[0] + index] = boat->symbol;
					}
					error = 1;
				} else {
					error = 0;
				}
			}
		} else {
			if (((boat->pos[1] + boat->length) >= sz_board[1]) || boat->pos[0] >= sz_board[0]) {
				error = 0;
			} else {
				//Check if all water
				int clear_water = 0;

				for (int index = 0; index < boat->length; index++) {
					if (board[boat->pos[0]][boat->pos[0] + index] != '*') {
						clear_water = 1;
					}
				}

				if (clear_water == 0) {
					for (int index = 0; index < boat->length; index++) {
						board[boat->pos[0] + index][boat->pos[0]] = boat->symbol;
					}
					error = 1;
				} else {
					error = 0;
				}
			}
		}
	}
}

void get_file_dimensions(FILE *source, int *x_coord, int *y_coord)
{
	char curChar;

	while((curChar = fgetc(source)) != EOF) { //Get size of board
		if (*x_coord == 0) {
			*y_coord += 1;
		}
		if (curChar == '\n' && curChar != EOF) {
			*x_coord += 1;
		}
	}

	*y_coord -= 1; //To compensate for the final row being read.
}
