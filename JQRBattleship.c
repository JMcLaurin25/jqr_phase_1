
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "gameboard.h"

int main (void)
{
	int gameloop = 0;

	while (gameloop == 0) {
		int sz_board[2] = {0}, usernum = 0;
		char **gamefield, *endptr, user_filename[24], userselect[4];

		printf("\n  Welcome to Battleship\n\n");

		printf("\t1. New Game\n\t2. Load Map\n\t3. Quit\n");

		fgets(userselect, 4, stdin);
		userselect[strlen(userselect) - 1] = '\0';

		usernum = strtol(userselect, &endptr, 10);

		switch (usernum) {
			case 1:
				printf("Enter Board dimensions: 12,12 >> ");
				guess(sz_board);//Get dimensions
				printf("SIZE: %dx%d\n", sz_board[0], sz_board[1]);
				printf("Making board...\n");
				gamefield = task1(sz_board[0], sz_board[1]); //Task 1 - create game board
				playgame(gamefield, sz_board);
				free_game(gamefield, sz_board);
				break;
			case 2:
				printf("Filename of map: ");
				fgets(user_filename, sizeof(user_filename), stdin);
				user_filename[strlen(user_filename) - 1] = '\0';
				gamefield = open_board(user_filename, sz_board); //Task 5
				playgame(gamefield, sz_board);
				free_game(gamefield, sz_board);
				break;
			case 3:
				gameloop = 1;
				break;
			default:
				printf("Invalid Entry!\n");
				break;
		}
	}

	printf("GOODBYE!\n");
}



