CFLAGS+=-std=c11 -Werror -Wno-deprecated -pedantic -Wall -Wextra -pedantic -Wwrite-strings -fstack-usage -Wstack-usage=256 -Wfloat-equal -Waggregate-return -Winline

all: JQRBattleship

JQRBattleship: JQRBattleship.o gameboard_functs.o gameboard.o

.PHONY: clean debug

clean:
	rm JQRBattleship *.o *.su

debug: CFLAGS+=-g
debug: all 

