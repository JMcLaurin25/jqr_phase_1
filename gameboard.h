#ifndef GAMEBOARD_H
#define GAMEBOARD_H

struct scores {
	int patrol;
	int submarine;
	int cruiser;
	int destroyer;
	int battleship;
	int aircraft_carrier;
};

char ** task1(int x_coord, int y_coord); //Task 1

void playgame (char **gamefield, int *sz_board);
void print_board(char **board, int x, int y);
void save_board(const char *target_file, char ** board, int x, int y);

char ** open_board(const char *target_file, int *sz_board);
void fire(char **board, struct scores *ship_health, int *x_coord, int *y_coord, int *sunk);

void guess(int *userguess);
void get_ship_health(char **board, int *sz_board, struct scores *ship_health);
void free_game(char ** board, int *sz_board);
#endif
